json.array!(@items) do |item|
  json.extract! item, :id, :title, :description, :category_id, :admin_user_id
  json.url item_url(item, format: :json)
end
