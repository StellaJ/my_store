class CartsController < InheritedResources::Base
before_action :set_cart, only: [:show, :edit, :update, :destroy]
rescue_from ActiveRecord::RecordNotFound, with: :invalid_cart

    def show

    end


    def index

    end

    def set_cart
        @cart = Cart.find(session[:cart_id])
        rescue ActiveRecord::RecordNotFound
        @cart = Cart.create
        session[:cart_id] = @cart.id
    end

    def invalid_cart
        logger.error "Attempt to access invalid cart #{params[:id]}"
        redirect_to store_url, notice: 'Invalid cart'
    end

    def destroy
        @cart.destroy if @cart.id == session[:cart_id]
            session[:cart_id] = nil
            respond_to do |format|
            format.html { redirect_to home_index_url,
        notice: 'Your cart is currently empty' }
            format.json { head :no_content }
    end
end
end

