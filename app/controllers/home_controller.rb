class HomeController < ApplicationController
	  before_action :authenticate_user!, only: [:create]
  def help
  end
  def index
    @search = Item.active.search(params[:q])
    @items = @search.result.paginate(:page => params[:page], :per_page => 30)
    @items = Item.order(:title)
  end
end
