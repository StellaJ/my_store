ActiveAdmin.register Item do

    controller do
        def permitted_params
           params.permit item: [:title, :description, :category_id, :image, :active, :price, :weight]
        end
    end
    index do 
		column :id
		column :title
		column :description
		column :created_at
		column :active
		column :price
		column :weight
		actions
	end
 
end

