ActiveAdmin.register Order do

 controller do
        def permitted_params
           params.permit order: [:name, :address, :email, :pay_type]
        end
    end
  index do 
    column :id
    column :name
    column :address
    column :email
    column :pay_type
    column :created_at
    column :updated_at
    actions
  end
  
end
