ActiveAdmin.register Cart do

    controller do
        def permitted_params
            params.permit cart: [:total_amount, :weight]
        end
    end
  
end
