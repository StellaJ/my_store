class Cart < ActiveRecord::Base
	has_many :line_items, dependent: :destroy
    has_many :items, :through => :line_items
    
	def add_item(item_id)
           current_item = line_items.find_by(item_id: item_id)
        if current_item
           current_item.quantity += 1
    else
        current_item = line_items.build(item_id: item_id)
    end
        current_item
    end

    def sum_quantity
    	line_items.map(&:quantity).sum
    end

    def sum_weight
       items.map(&:weight).sum
    end

    def total_price
        line_items.to_a.sum { |item| item.total_price }
    end

end
