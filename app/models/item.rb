class Item < ActiveRecord::Base
  belongs_to :category
  has_many :line_items
  has_many :carts, :through => :line_items
  has_many :orders, through: :line_items
  before_destroy :ensure_not_referenced_by_any_line_item
  
  has_attached_file :image, :styles => { :medium => "300x300>", :thumb => "100x100>", :cropped => "200x200#" }, :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/

  after_validation(on: :create) do 
   self.active = false  	
  end

  scope :active, -> {where active:true}
  scope :inactive, -> {where active:false}
  
  self.per_page = 10

  def to_s
    title
  end

  private

  def ensure_not_referenced_by_any_line_item
    if line_items.empty?
      return true
    else
      errors.add(:base, 'Line Items present')
    return false
  end
end
end
