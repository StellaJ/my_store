MyStore::Application.routes.draw do
  
  resources :orders

  resources :line_items

  resources :carts

  resources :categories


  resources :items do
  	collection do
  		get 'inactive'
  	end
  end


  get "home/index"
  get "home/author"
  get "home/chantry"
  get "home/help"
  get "carts/show"
  root 'items#index'

  devise_for :users
  ActiveAdmin.routes(self)

end
