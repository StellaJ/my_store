# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
MyStore::Application.config.secret_key_base = 'c83f86e97b55bd5143d1d69e503aff02ea6180f325de9afffa3157cede8911190a689e78bd0fd717e686fa7aaf47a7a846e55df6e112b13bc95d62c15e5d3305'
