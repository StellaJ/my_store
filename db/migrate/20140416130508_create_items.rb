class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.string :title
      t.text :description
      t.integer :category_id
      t.references :admin_user, index: true

      t.timestamps
    end
  end
end
