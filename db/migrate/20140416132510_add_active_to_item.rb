class AddActiveToItem < ActiveRecord::Migration
  def change
    add_column :items, :active, :boolean
    Item.update_all(active: false)
  end
end
