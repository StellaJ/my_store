class CreateCarts < ActiveRecord::Migration
  def change
    create_table :carts do |t|
      t.integer :total_amount
      t.integer :weight

      t.timestamps
    end
  end
end
