# encoding: utf-8
Category.destroy_all
Item.destroy_all

User.create(email: "admin@example.com", password: "1234", password_confirmation: "1234", admin: true)
User.create(email: "yola@example.com", password: "5678", password_confirmation: "5678", admin: false)
cat = Category.create(title: "Ze sznurka")
cat1 = Category.create(title: "Z drewna")
cat2 = Category.create(title: "Ze szkła")
Item.create(title: "Drzewko", description: "drzewko ze sznurka", category: cat)
Item.create(title: "Kurka", description: "kurka z drewna", category: cat1)
Item.create(title: "Anioł", description: "anioł ze szkła", category: cat2)
Item.create(title: "Gruszka", description: "gruszka ze szkła", category: cat2)
Item.create(title: "Wiewiórka", description: "wiewiórka z drewna", category: cat1)